<!DOCTYPE html>
<html lang="en">
<head>
    <title>JWL Solution llc</title>
    <meta charset="utf-8">
    <meta name="description" content="Piksell Responsive Multipurpose Template">
    <meta name="author" content="Diamant Gjota" />
    <meta name="keywords" content="piksell, html5, css3, template, bootstrap" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Icons/FontAwesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- totop -->
    <link rel="stylesheet" href="css/ui.totop.css" />
    <!-- owl carusol -->
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <!-- zetta menu -->
    <link rel="stylesheet" href="css/zetta-menu.min.css" />
    <!-- map style -->
    <link rel="stylesheet" href="css/map.css" />
    <!-- Customizable CSS -->
    <link rel="stylesheet" href="css/style.css" />
    
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="img/favicon.ico">-->
    
    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>

<!--- Set the length of the text string for the CAPTCHA image. ---> 
<cfset stringLength=6> 
<!--- Specify the list of characters used for the random text string. The following list 
    limits the confusion between upper- and lowercase letters as well as between numbers and 
    letters. ---> 
<cfset 
    stringList="2,3,4,5,6,7,8,9,a,b,d,e,f,g,h,j,n,q,r,t,y,A,B,C,D,E,F,G,H,K,L,M,N,P,Q,R,S, 
    T,U,V,W,X,Y,Z"> 
<cfset rndString=""> 
<!--- Create a loop that builds the string from the random characters. ---> 
<cfloop from="1" to="#stringLength#" index="i"> 
    <cfset rndNum=RandRange(1,listLen(stringList))> 
    <cfset rndString=rndString & listGetAt(stringList,rndNum)> 
</cfloop> 
<!--- Hash the random string. ---> 
<cfset rndHash=Hash(rndString)> 

<body>

	<!-- top header -->
    <div id="top-header" class="dark">
        <div class="container top-header">
            <div class="row">
                
                <div class="col-md-6 col-sm-7">
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-facebook facebook-icon-jump"></i>
                            <i class="fa fa-facebook social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-twitter twitter-icon-jump"></i>
                            <i class="fa fa-twitter social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-linkedin linkedin-icon-jump"></i>
                            <i class="fa fa-linkedin social-icon-jump-dark"></i>
                        </div>
                    </a>
                </div>
                
            </div><!-- end row -->
        </div><!-- end container -->
    </div>
    <!-- end top header -->
    
    <!-- start Navigation -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <!-- Zetta Menu 1 -->
        <ul onClick="" class="zetta-menu zm-response-switch zm-full-width zm-effect-slide-top">
            <!-- LOGO -->
            <li class="zm-logo">
                <a href="index.cfm"><img src="img/JWL/jwl_logo.png" alt="JWL logo" /></a>
                
            </li>
            <!-- end LOGO -->
            
            <!-- Home Menu -->
            <li><a href="index.cfm">Home</a></li>
            <!-- /Home Menu -->
            
            <!-- Features Menu -->
            <li><a href="solutions.cfm">Solutions</a></li>
            <!-- /Features Menu -->
            
            <!-- Pages -->
			<li><a href="#">News</a></li>
            <!-- /Pages -->

            <!-- Contact Element -->
            <li><a href="#">Contact</a></li>
            <!-- /Contact Element -->
            
            <!-- Blog Menu -->
            <!--<li><a href="documentation/docs.html">Blog</a></li>-->
            <!-- /Blog Menu -->
    
        </ul>
        <!-- /Zetta Menu 1 -->
      </div><!-- /.container-fluid -->
    </nav>
    <!-- end nav -->
	
    <!-- header -->
    <header id="header" class="section-parallax parallaxBg" style="background-image: url(img/contact-section-bg.jpg);">
    	<div class="layer"></div>
        <div class="container parallax-content text-center">
        	<h1>How to <span class="piksell-color">contact</span> us</h1>
        </div><!-- end container -->
    </header>
    <!-- end header -->
    
    <!-- contact section -->
    <section class="content">
    	<div class="container">
        	<div class="row">
                
                <!---<div class="col-md-5 padd">
                	<h5 class="heading heading-v3"><span>Visit Us</span></h5>
                    <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.</p>
                    <hr class="divider-dashed" />
                    <address>
                        <p> 
                            <i class="fa fa-map-marker"></i>&nbsp;77 Mass. Ave., E14/E15<br>
                            Seattle, MA 02139-4307 USA <br>
                            <i class="fa fa-phone"></i>&nbsp;012.345.6789 <br>
                            <i class="fa fa-envelope"></i>&nbsp;<a href="mailto:mantii_ju@hotmail.com">info@piksell.com</a> 
                        </p>
                    </address>
                    <hr class="divider-dashed" />
                    Monday - Friday: <strong>9:00 am - 5:00 pm </strong><br />Saturday - Sunday: <strong>Closed</strong>
                </div>--->
                <!-- end col-md-5 -->
               
                <div class="col-md-5 padd">
                	<h5 class="heading heading-v3"><span>Get in touch</span></h5>
                    <form role="form" id="contactMe" action="contactRequest.cfm">
                        <div class="form-group">
                            <input type="text" class="form-control" name="fullName" id="fullName" placeholder="Name">
                        </div>
                        
                        <div class="form-group">
                            <input type="text" class="form-control" name="contactEmail" id="contactEmail" placeholder="Email">
                        </div>
                        
                        <div class="form-group">
                            <textarea class="form-control" name="message" id="message" placeholder="Message" rows="6"></textarea>
                        </div>
                        <div class="form-group">Type what you see
                            <input type="text" class="form-control" name="userInput" id="userInput">
                        </div>

						<cfimage action="captcha" fontSize="24" fonts="Times New Roman" width="200" height="50" 
						    text="#rndString#">

		               <div class="form-actions">
							<button type="submit" class="btn btn-lg pi-btn-default btn-block rounded">Send</button>
						</div>
						
						<!---       
                        <div class="form-group">
                            <a href="#" class="btn btn-lg pi-btn-default btn-block rounded">Send</a>
                        </div>
                        --->
                	</form><!-- end form -->

                </div><!-- end col-md-7 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section>
    <!-- end contact section -->
    
    <!-- footer -->
    <footer id="footer" class="dark">
        <div class="container footer">
            <div class="row">
                
                <div class="col-sm-5 padd">
                    <h5>JWL Solutions, LLC.</h5>
                    <div class="line-left"></div>
                    <p>JWL Solutions offers business solutions ranging from management applications to company
                    websites.</p>
                </div>
                
                <div class="col-sm-3 padd">
                    <h5>&nbsp;</h5>
                </div>
                
                <div class="col-sm-4 padd">
                   <h5>Contact Us</h5>
                    <div class="line-left"></div>
                    <address>
                        <p> 
                            <!--<i class="fa fa-map-marker"></i>&nbsp;123 Test Street<br>
                            Atlanta, GA 30349 USA <br>-->
                            <i class="fa fa-phone"></i>&nbsp;404.313.5494 <br>
                            <i class="fa fa-envelope"></i>&nbsp;<a href="mailto:mail@jwlsolution.com">mail@jwlsolution.com</a> 
                        </p>
                    </address>
                   <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-facebook facebook-icon-jump"></i>
                            <i class="fa fa-facebook social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-twitter twitter-icon-jump"></i>
                            <i class="fa fa-twitter social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-linkedin linkedin-icon-jump"></i>
                            <i class="fa fa-linkedin social-icon-jump-dark"></i>
                        </div>
                    </a>
                </div>
            
            </div><!-- end row -->
        </div><!-- end container -->
        
        <div class="copyrights">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <p class="pull-left">2015 &copy; JWL Solutions, LLC / All Rights reserved.</p>
                        <!---
                        <ul class="inline-block pull-right">
                            <li><a href="privacy-policy.html">Privacy Policy</a></li>  
                            <li class="topbar-devider"></li>
                            <li><a href="faq.html">Terms of Service</a></li>
                        </ul>
                        --->
                    </div>
                    
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </footer>
    <!-- end footer -->
    
   	<!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
   	<script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.ui.totop.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/gmaps.js"></script>
    <script type="text/javascript">
		//map
		var map;
		$(document).ready(function(){
				map = new GMaps({
				el: '#map',
				lat: -12.043333,
				lng: -77.028333
			});
			map.drawOverlay({
				lat: map.getCenter().lat(),
				lng: map.getCenter().lng(),
				layer: 'overlayLayer',
				content: '<div class="overlay">Piksell<div class="overlay_arrow above"></div></div>',
				verticalAlign: 'top',
				horizontalAlign: 'center'
			});
		});
	</script>
    
</body>
</html>
