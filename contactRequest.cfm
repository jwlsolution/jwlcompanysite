<cfparam name="fullName" default="">
<cfparam name="contactEmail" default="">
<cfparam name="message" default="">

<cfmail to = "wilkins@jwlsolution.com" from = "#contactEmail#" subject = "New Inquiry">
#fullName# has sent a request, please respond ASAP.
</cfmail>    

<cfmail to = "#contactEmail#" from = "wilkins@jwlsolution.com" subject = "Thank you for reaching out!">
#fullName#, Thank you for getting in touch!

We appreciate you contacting us. We will respond within 24 hours.

Have a great day!
</cfmail>

<cflocation url="index.cfm">