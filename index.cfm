<!DOCTYPE html>
<html lang="en">
<head>
    <title>JWL Solution llc</title>
    <meta charset="utf-8">
    <meta name="description" content="JWL Solutions LLC">
    <meta name="author" content=" " />
    <meta name="keywords" content="piksell, html5, css3, template, bootstrap" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Icons/FontAwesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- totop -->
    <link rel="stylesheet" href="css/ui.totop.css" />
    <!-- owl carusol -->
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <!-- zetta menu -->
    <link rel="stylesheet" href="css/zetta-menu.min.css" />
    <!-- layerSlider -->
    <link rel="stylesheet" href="css/layerslider.css" />
    <!-- Animation -->
    <link rel="stylesheet" href="css/animate.css" />
    <!-- Customizable CSS -->
    <link rel="stylesheet" href="css/style.css" />
    
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="img/favicon.ico">-->
    
    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
		
<body>

	<!-- top header -->
    <div id="top-header" class="dark">
        <div class="container top-header">
            <div class="row">
                
                <div class="col-md-6 col-sm-7">
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-facebook facebook-icon-jump"></i>
                            <i class="fa fa-facebook social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-twitter twitter-icon-jump"></i>
                            <i class="fa fa-twitter social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-linkedin linkedin-icon-jump"></i>
                            <i class="fa fa-linkedin social-icon-jump-dark"></i>
                        </div>
                    </a>
                </div>
                
            </div><!-- end row -->
        </div><!-- end container -->
    </div>
    <!-- end top header -->
    
    <!-- start Navigation -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <!-- Zetta Menu 1 -->
        <ul onClick="" class="zetta-menu zm-response-switch zm-full-width zm-effect-slide-top">
            <!-- LOGO -->
            <li class="zm-logo">
                <a href="#"><img src="img/JWL/jwl_logo.png" alt="JWL logo" /></a>
                
            </li>
            <!-- end LOGO -->
            
            <!-- Home Menu -->
            <li><a href="#">Home</a></li>
            <!-- /Home Menu -->
            
            <!-- Features Menu -->
            <li><a href="solutions.cfm">Solutions</a></li>
            <!-- /Features Menu -->
            
            <!-- Pages -->
            <li><a href="#">News</a></li>
            <!-- /Pages -->

            <!-- Contact Element -->
            <li><a href="contact.cfm">Contact</a></li>
            <!-- /Contact Element -->
            
            <!-- Blog Menu -->
            <!--<li><a href="documentation/docs.html">Blog</a></li>-->
            <!-- /Blog Menu -->   
        </ul>
        <!-- /Zetta Menu 1 -->
      </div><!-- /.container-fluid -->
    </nav>
    <!-- end nav -->
    
    <!-- layerSlider -->
    <div id="full-slider-wrapper">
		<div id="layerslider" style="width:100%;height:530px;">
			
             <div class="ls-slide" data-ls="transition2d:95;timeshift:-1000;">
				<img src="sliderimages/bg6b.jpg" class="ls-bg" alt="Slide background"/>
				
                <p class="ls-l" style="top:150px;left:50%;font-weight: 300;font-size:36px;line-height:37px;color:#21252b;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;">
					JWL SOLUTIONS
				</p>
				
                <p class="ls-l" style="top:210px;left:50%;font-family:'Indie Flower';font-size:18px;color:#384147;white-space: nowrap;" data-ls="offsetxin:0;durationin:1000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-400;">
					Solution Oriented Management System
				</p>
                <!---Clean And Creative Design With Dedicated Support System--->                
			</div><!-- end ls-slider -->
           
            <div class="ls-slide" data-ls="transition2d:95;timeshift:-1000;">
                <img src="sliderimages/slide-b-bg.jpg" class="ls-bg" alt="Slide background"/>
                
                <img class="ls-l" style="top:50px;left:125px;white-space: nowrap;" data-ls="offsetxin:400;durationin:2000;offsetxout:400;" src="sliderimages/sliderman.png" alt="">
				
                <p class="ls-l" style="top:80px;left:730px;font-weight: 300;height:40px;padding-right:10px;padding-left:10px;font-size:36px;line-height:37px;color:#fff;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-400;durationout:1000;">
					Powerful Tool<br />&amp; Clean Design
				</p>
				
                <p class="ls-l" style="top:170px;left:740px;font-family:'Indie Flower';font-size:22px;color:#fff;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-600;">
					Ideal for your Company
				</p>

                <p class="ls-l" style="top:210px;left:740px;font-family:'Indie Flower';font-size:18px;color:#fff;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-600;">
					Innovative, modern development enviornment<br />provides responsive interface.
				</p>                
			</div><!-- end ls-slider -->
        </div>
	</div>
    <!-- end layerSlider -->
    
    <!-- what-we-do -->
    <section id="what-we-do" class="content">
    	<div class="container what-we-do">
        	<div class="row">
            	
                <div class="col-md-3 col-sm-6 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box grey">
                            <i class="fa fa-users"></i>
                        </div>
                        <h3>Customer Centric</h3>
                        <p class="introtext">
                        	A customer first atmosphere allows us to focus on your critical requirements 
                        	that will cover the organizational map and ensure an easier management style.
                        </p>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box pink">
                            <i class="fa fa-line-chart"></i>
                        </div>
                        <h3>Organic Growth</h3>
                        <p class="introtext">
                        	Allowing customers the ability to make internal organization a lower priority
                        	to focus on their own growth and marketing.
                        </p>
                    </div>
                </div> 
                
                <div class="col-md-3 col-sm-6 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box red">
                            <i class="fa fa-lightbulb-o"></i>
                        </div>
                        <h3>Innovation</h3>
                        <p class="introtext">Keeping with ever evolving trends in the progamming languages,
                        security and database management.</p>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box purple">
                            <i class="fa fa-code-fork"></i>
                        </div>
                        <h3>Personal Design</h3>
                        <p class="introtext">Give your management application the personal touch that will fit
                        the culture and prefrence of your team.</p>
                    </div>
                </div>

            </div><!-- end of row -->
        </div><!-- end of container -->
    </section>
    <!-- end of what-we-do -->
    
    <!-- why-we-are-the-best -->
 	<section class="content section-grey">
    	<div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 padd">
                    
                    <div class="owl-carousel owl-item-1">

                        <div class="item">
                            <img src="img/portfolio/technology/image_02.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="img/portfolio/technology/image_01.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="img/portfolio/technology/image_03.jpg" alt="" />
                        </div>

                    </div><!-- end carousel-v3 -->
                    
                </div><!-- end col-sm-6 -->
                
                <div class="our-skills col-sm-12 col-md-6 padd">
                    <h4 class="heading heading-v3"><span>The <b class="piksell-color">Edge</b></span></h4>
                    
                   	<div class="col-sm-12 padd">
                        <div class="left_icons border-color2 wow fadeIn">
                            <div class="single_box_left default">
                                <i class="fa fa-rocket"></i>
                            </div>
                            <div class="single_box_right">
                                <h3>Premium Service</h3>
                                We understand the importance of H2H relationships between a vendor and customer.
                                It's critical for our customers to operate on a current platform and use a management
                                tool that is secure, stable and modern 
                            </div>
                        </div><!-- left-icon -->
                    </div><!-- end col-sm-12 -->
                    
                    <div class="col-sm-12 padd">
                        <div class="left_icons border-color2 wow fadeIn">
                            <div class="single_box_left default">
                                <i class="fa fa-lightbulb-o"></i>
                            </div>
                            <div class="single_box_right">
                                <h3>24/7 Support</h3>
                                We offer a 24 hour support to all customers and can be contacted through multiple
                                streams of communication.  Contact support from the online application, email
                                or phone.
                            </div>
                        </div><!-- left-icon -->
                    </div><!-- end col-sm-12 -->

                </div><!-- end our skills -->
                
            </div><!-- end row -->
		</div><!-- end container -->        
    </section>
    <!-- end why-we-are-the-best -->
    
    <!-- about-us-section -->
    <section class="content">
    	<div class="container">
        	<div class="row">
            	<h4 class="text-center">About <b class="piksell-color">Us</b></h4>
                <div class="line"></div>                                
            	<div class="top-description-text">
            		<p style="font-size:14pt;text-align:justify;">We specialize in creating customized software for your business.  Using the latest
            		development technology your system is accessible from anywhere and on multiple devices.
            		By learning and understnding your business mission and goal, we customize your management
            		system and website to deliver big value to you in ways of improved revenue, reduced costs
            		and eliminating repetitive operational work.</p>
                </div>

            </div><!-- end row -->

        </div><!-- end container -->
    </section>
    <!-- end about-us-section -->

    
    <!-- footer -->
    <footer id="footer" class="dark">
        <div class="container footer">
            <div class="row">
                
                <div class="col-sm-5 padd">
                    <h5>JWL Solutions, LLC.</h5>
                    <div class="line-left"></div>
                    <p>JWL Solutions offers business solutions ranging from management applications to company
                    websites.</p>
                </div>
                
                <div class="col-sm-3 padd">
                    <h5>&nbsp;</h5>
                </div>
                
                <div class="col-sm-4 padd">
                   <h5>Contact Us</h5>
                    <div class="line-left"></div>
                    <address>
                        <p> 
                            <!--<i class="fa fa-map-marker"></i>&nbsp;123 Test Street<br>
                            Atlanta, GA 30349 USA <br>-->
                            <i class="fa fa-phone"></i>&nbsp;404.313.5494 <br>
                            <i class="fa fa-envelope"></i>&nbsp;<a href="mailto:mail@jwlsolution.com">mail@jwlsolution.com</a> 
                        </p>
                    </address>
                   <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-facebook facebook-icon-jump"></i>
                            <i class="fa fa-facebook social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-twitter twitter-icon-jump"></i>
                            <i class="fa fa-twitter social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-linkedin linkedin-icon-jump"></i>
                            <i class="fa fa-linkedin social-icon-jump-dark"></i>
                        </div>
                    </a>
                </div>
            
            </div><!-- end row -->
        </div><!-- end container -->
        
        <div class="copyrights">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <p class="pull-left">2018 &copy; JWL Solutions, LLC / All Rights reserved.</p>
                        <!---
                        <ul class="inline-block pull-right">
                            <li><a href="privacy-policy.html">Privacy Policy</a></li>  
                            <li class="topbar-devider"></li>
                            <li><a href="faq.html">Terms of Service</a></li>
                        </ul>
                        --->
                    </div>
                    
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </footer>
    <!-- end footer -->
    
    <!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.ui.totop.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/greensock.js"></script>
    <script src="js/layerslider.transitions.js"></script>
    <script src="js/layerslider.kreaturamedia.jquery.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
    
    <script>
		jQuery("#layerslider").layerSlider({
			responsive: false,
			responsiveUnder: 1280,
			layersContainer: 1280,
			skin: 'noskin',
			hoverPrevNext: false,
			skinsPath: '../layerslider/skins/'
		});
	</script>
    
    <script>
		//Animation Wow.js
		new WOW().init();
	</script>
    
</body>
</html>
