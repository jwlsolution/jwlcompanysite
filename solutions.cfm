<!DOCTYPE html>
<html lang="en">
<head>
    <title>JWL Solution llc</title>
    <meta charset="utf-8">
    <meta name="description" content="JWL Solutions LLC">
    <meta name="author" content="Diamant Gjota" />
    <meta name="keywords" content="piksell, html5, css3, template, bootstrap" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Icons/FontAwesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- totop -->
    <link rel="stylesheet" href="css/ui.totop.css" />
    <!-- owl carusol -->
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <!-- zetta menu -->
    <link rel="stylesheet" href="css/zetta-menu.min.css" />
    <!-- Animation -->
    <link rel="stylesheet" href="css/animate.css" />
    <!-- Customizable CSS -->
    <link rel="stylesheet" href="css/style.css" />
    
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="img/favicon.ico">-->
    
    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
		
<body>
    
	<!-- top header -->
    <div id="top-header" class="dark">
        <div class="container top-header">
            <div class="row">
                
                <div class="col-md-6 col-sm-7">
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-facebook facebook-icon-jump"></i>
                            <i class="fa fa-facebook social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-twitter twitter-icon-jump"></i>
                            <i class="fa fa-twitter social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x2">
                        <div>
                            <i class="fa fa-linkedin linkedin-icon-jump"></i>
                            <i class="fa fa-linkedin social-icon-jump-dark"></i>
                        </div>
                    </a>
                </div>
            
            </div><!-- end row -->
        </div><!-- end container -->
    </div>
    <!-- end top header -->
    
    <!-- start Navigation -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <!-- Zetta Menu 1 -->
        <ul onClick="" class="zetta-menu zm-response-switch zm-full-width zm-effect-slide-top">
            <!-- LOGO -->
            <li class="zm-logo">
                <a href="index.cfm"><img src="img/JWL/jwl_logo.png" alt="JWL logo" /></a>
                
            </li>
            <!-- end LOGO -->
            
            <!-- Home Menu -->
            <li><a href="index.cfm">Home</a></li>
            <!-- /Home Menu -->
            
            <!-- Solutions Menu -->
            <li><a href="##">Solutions</a></li>
            <!-- /Solutions Menu -->
            
            <!-- Pages -->
            <li><a href="#">News</a></li>
            <!-- /Pages -->

            <!-- Contact Element -->
            <li><a href="contact.cfm">Contact</a></li>
            <!-- /Contact Element -->
            
            <!-- Blog Menu -->
            <!--<li><a href="documentation/docs.html">Blog</a></li>-->
            <!-- /Blog Menu -->
        </ul>
        <!-- /Zetta Menu 1 -->
      </div><!-- /.container-fluid -->
    </nav>
    <!-- end nav -->
    
    <!-- header -->
    <header id="header" class="section-parallax parallaxBg" style="background-image: url(img/image_01.jpg);">
    	<div class="layer"></div>
        <div class="container parallax-content text-center">
        	<h1>What <span class="piksell-color">We</span> Offer</h1>
            <p>Allow us the opportunity to service you</p>
        </div><!-- end container -->
    </header>
    <!-- end header -->
 
     <!-- get in touch -->
    <section class="content" style="background-image: url(img/pixel_weave.png);">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12 text-center">
                	<h5 class="weight-100 margin-bottom-25 wow fadeInDown">Contact us for a Demo!</h5>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section>
    <!-- end get in touch -->
       
    <!-- service section -->
    <section id="service" class="content">
    	<div class="container">
        	<div class="row">
            	<div class="top-description-text">
            		System Management applications
            		<p>Subscription application @ $35 monthly</p>
            		<p>Our features</p>
                </div>
            	<div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box grey">
                            <i class="fa fa-home"></i>
                        </div>
                        <h3>Dashboard</h3>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box pink">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <h3>Scheduling</h3>
                    </div>
                </div> 
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box red">
                            <i class="fa fa-file-text-o"></i>
                        </div>
                        <h3>Reporting</h3>
                    </div>
                </div>          

                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box purple">
                            <i class="fa fa-comments-o"></i>
                        </div>
                        <h3>Internal Notifiation Between Employees</h3>
                    </div>
                </div>          

                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box orange">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <h3>Email Customers</h3>
                    </div>
                </div>          

                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box blue">
                            <i class="fa fa-tablet"></i>
                        </div>
                        <h3>Mobile & Tablet Friendly</h3>
                    </div>
                </div>          
            </div><!-- end row -->

        	<div class="row">
            	<div class="top-description-text">
            		Websites starting at $500 and up.
            		<p>See &aacute; la carte menu to create your package.</p>
                </div>
            	<div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box grey">
                            <i class="fa fa-home"></i>
                        </div>
                        <h3>Home Page</h3>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box pink">
                            <i class="fa fa-tablet"></i>
                        </div>
                        <h3>Product or Services</h3>
                    </div>
                </div> 
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box red">
                            <i class="fa fa-newspaper-o"></i>
                        </div>
                        <h3>News or About Us</h3>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box purple">
                            <i class="fa fa-list"></i>
                        </div>
                        <h3>Blog</h3>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box orange">
                            <i class="fa fa-picture-o"></i>
                        </div>
                        <h3>Photos</h3>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box purple">
                            <i class="fa fa-usd"></i>
                        </div>
                        <h3>Request Quotes</h3>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box blue">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <h3>Links to Social Media</h3>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 padd">
                    <div class="single_box style7">
                        <div class="icons_introimg image-icon box red">
                            <i class="fa fa-signal"></i>
                        </div>
                        <h3>Misc</h3>
                        <p class="introtext">Any speicifc page you may need</p>
                    	<!--href="#miscInfoModal" -->
                    	<a class="btn btn-primary rounded pi-btn-light-grey"
                    	data-toggle="modal" data-target="#miscInfoModal">Learn More</a>
                    </div>
                    <!-- Misc Info Modal -->
					<cfinclude template="Modal/MiscInfo.cfm">
                </div>                
            </div><!-- end row -->
        </div><!-- end container -->
    </section>
    <!-- end service section -->
     
    <!-- get in touch -->
    <section class="content" style="background-image: url(img/pixel_weave.png);">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12 text-center">
                	<h5 class="weight-100 margin-bottom-25 wow fadeInDown">Want to contact us? <span class="weight-500">Feel free.</span></h5>
                    <a href="contact.cfm" class="btn btn-lg rounded pi-btn-default wow fadeInUp"><i class="fa fa-envelope-o"></i> Get in touch</a>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section>
    <!-- end get in touch -->

    <!-- footer -->
    <footer id="footer" class="dark">
        <div class="container footer">
            <div class="row">
                
                <div class="col-sm-5 padd">
                    <h5>JWL Solutions, LLC.</h5>
                    <div class="line-left"></div>
                    <p>JWL Solutions offers business solutions ranging from management applications to company
                    websites.</p>
                </div>
                
                <div class="col-sm-3 padd">
                    <h5>&nbsp;</h5>
                </div>
                
                <div class="col-sm-4 padd">
                   <h5>Contact Us</h5>
                    <div class="line-left"></div>
                    <address>
                        <p> 
                            <!--<i class="fa fa-map-marker"></i>&nbsp;123 Test Street<br>
                            Atlanta, GA 30349 USA <br>-->
                            <i class="fa fa-phone"></i>&nbsp;404.313.5494 <br>
                            <i class="fa fa-envelope"></i>&nbsp;<a href="mailto:mail@jwlsolution.com">mail@jwlsolution.com</a> 
                        </p>
                    </address>
                   <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-facebook facebook-icon-jump"></i>
                            <i class="fa fa-facebook social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-twitter twitter-icon-jump"></i>
                            <i class="fa fa-twitter social-icon-jump-dark"></i>
                        </div>
                    </a>
                    <a href="#" class="social-icon-jump-x4 rounded">
                        <div>
                            <i class="fa fa-linkedin linkedin-icon-jump"></i>
                            <i class="fa fa-linkedin social-icon-jump-dark"></i>
                        </div>
                    </a>
                </div>
            
            </div><!-- end row -->
        </div><!-- end container -->
        
        <div class="copyrights">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <p class="pull-left">2015 &copy; JWL Solutions, LLC / All Rights reserved.</p>
                        <!---
                        <ul class="inline-block pull-right">
                            <li><a href="privacy-policy.html">Privacy Policy</a></li>  
                            <li class="topbar-devider"></li>
                            <li><a href="faq.html">Terms of Service</a></li>
                        </ul>
                        --->
                    </div>
                    
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </footer>
    <!-- end footer -->
        
    <!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.ui.totop.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
    
    <script>
		//Animation Wow.js
		new WOW().init();
	</script>
    
</body>
</html>
